import { StatusBar } from 'expo-status-bar';
import { Button, StyleSheet, SafeAreaView } from 'react-native';

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <Button
        color="gray"
        title="Verbal Portion" onPress={() => console.log("Button tapped")} />

      <Button
        color="gray"
        title="Written Portion" onPress={() => console.log("Button tapped")} />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
